﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class BangHoaDonController : Controller
    {
        private CS4PEntities db = new CS4PEntities();

        // GET: /BangHoaDon/
        public ActionResult Index()
        {
            return View(db.BangHoaDons.ToList());
        }

        // GET: /BangHoaDon/Create
        public ActionResult Create()
        {
            return View(Session["BangHoaDon"]);
        }

        public ActionResult Details1(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BangHoaDon banghoadon = db.BangHoaDons.Find(id);
            if (banghoadon == null)
            {
                return HttpNotFound();
            }
            return View("Details", banghoadon);
        }
        // POST: /BangHoaDon/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BangHoaDon model)
        {
            if (ModelState.IsValid)
            {
                Session["BangHoaDon"] = model;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create2()
        {
            using (var scope = new TransactionScope())
            try
            {
                var bangHoaDon = Session["BangHoaDon"] as BangHoaDon;
                var CTHoaDon = Session["CTHoaDon"] as List<ChiTietHD>;
                
                db.BangHoaDons.Add(bangHoaDon);
                db.SaveChanges();

                foreach (var chiTiet in CTHoaDon)
                {
                    chiTiet.HD_id = bangHoaDon.id;
                    chiTiet.BangSanPham = null;
                    db.ChiTietHDs.Add(chiTiet);
                }
                db.SaveChanges();
                scope.Complete();

                Session["BangHoaDon"] = null;
                Session["CTHoaDon"] = null;
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }
            return View("Create");
        }

        // GET: /BangHoaDon/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BangHoaDon banghoadon = db.BangHoaDons.Find(id);
            if (banghoadon == null)
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                Session["BangHoaDon"] = banghoadon;
            }
            return View(banghoadon);
        }

        // POST: /BangHoaDon/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BangHoaDon banghoadon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(banghoadon).State = EntityState.Modified;
                Session["BangHoaDon"] = banghoadon;
                db.SaveChanges();
                return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
            }
            return View(banghoadon);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit2(BangHoaDon banghoadon)
        {
            //if (ModelState.IsValid)
            //{
                //db.Entry(banghoadon).State = EntityState.Modified;
                //db.SaveChanges();
                //return RedirectToAction("Index");

            using (var scope = new TransactionScope())
                    try
                    {
                        var bangHoaDon = Session["BangHoaDon"] as BangHoaDon;
                        var CTHoaDon = Session["CTHoaDon"] as List<ChiTietHD>;

                        db.Entry(banghoadon).State = EntityState.Modified;
                        db.SaveChanges();

                        foreach (var chiTiet in CTHoaDon)
                        {
                            chiTiet.HD_id = bangHoaDon.id;
                            chiTiet.BangSanPham = null;
                            db.ChiTietHDs.Add(chiTiet);
                        }
                        
                        db.SaveChanges();
                        scope.Complete();

                        Session["BangHoaDon"] = null;
                        Session["CTHoaDon"] = null;
                        TempData["message"] = "Chỉnh sửa hóa đơn thành công.";
                    return RedirectToAction("Index");
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e.Message);
                    }
                return View("Edit");
            //}
        }

        // GET: /BangHoaDon/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BangHoaDon banghoadon = db.BangHoaDons.Find(id);
            if (banghoadon == null)
            {
                return HttpNotFound();
            }
            return View(banghoadon);
        }

        // POST: /BangHoaDon/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //BangHoaDon banghoadon = db.BangHoaDons.Find(id);
            //ChiTietHD chitiethd = db.ChiTietHDs.Find(id);
            //db.BangHoaDons.Remove(banghoadon);
            //db.ChiTietHDs.Remove(chitiethd);
            //db.SaveChanges();
            //return RedirectToAction("Index");

            if (db.ChiTietHDs.Where(c => c.HD_id == id).ToList().Count > 0)
            {
                TempData["message"] = "Không thể xóa hóa đơn có chứa sản phẩm.";
                return RedirectToAction("Index");
            }
            else
            {               
                BangHoaDon banghoadon = db.BangHoaDons.Find(id);
                db.BangHoaDons.Remove(banghoadon);
                db.SaveChanges();
                TempData["message"] = "Xóa hóa đơn thành công.";
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
