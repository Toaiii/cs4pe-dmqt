﻿
using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication.Controllers;
using System.Web.Mvc;
using WebApplication.Models;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using System.Transactions;
using Moq;
namespace WebApplication.Tests.Controllers
{
    [TestClass]
    public class BangHoaDonTest
    {
        [TestMethod]
        public void TestDetails()
        {
            var controller = new BangHoaDonController();
            var db = new CS4PEntities();
            foreach (var item in db.BangHoaDons)
            {
                var result = controller.Details1(item.id) as ViewResult;

                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void TestIndex()
        {
            var controller = new BangHoaDonController();
            var result = controller.Index() as ViewResult;
            var db = new CS4PEntities();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(List<BangHoaDon>));
            Assert.AreEqual(db.BangHoaDons.Count(), ((List<BangHoaDon>)result.Model).Count);
        }
    }
}