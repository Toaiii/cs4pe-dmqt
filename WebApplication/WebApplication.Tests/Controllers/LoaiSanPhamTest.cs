﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication.Controllers;
using System.Web.Mvc;
using WebApplication.Models;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using System.Transactions;
using Moq;

namespace WebApplication.Tests.Controllers
{
    [TestClass]
    public class LoaiSanPhamTest
    {
        [TestMethod]
        public void TestIndex()
        {
            var controller = new LoaiSanPhamController();
            var result = controller.Index() as ViewResult;
            var db = new CS4PEntities();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(List<LoaiSanPham>));
            Assert.AreEqual(db.LoaiSanPhams.Count(), ((List<LoaiSanPham>)result.Model).Count);
        }

        [TestMethod]
        public void TestDetail()
        {
            var controller = new LoaiSanPhamController();
            var db = new CS4PEntities();
            foreach (var item in db.LoaiSanPhams)
            {
                var result = controller.Details(item.id) as ViewResult;

                Assert.IsNotNull(result);
            }
        }

        [TestMethod]
        public void TestCreate1()
        {
            var controller = new LoaiSanPhamController();
            var result = controller.Create() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestCreate2()
        {
            var controller = new LoaiSanPhamController();
            var db = new CS4PEntities();
            var create = new LoaiSanPham();
            create.TenLoai = "TenLSP";

            var result1 = controller.Create(create) as RedirectToRouteResult;
            Assert.AreEqual("Index", result1.RouteValues["action"]);
        }

        [TestMethod]
        public void TestEdit()
        {
            var controller = new LoaiSanPhamController();
            var db = new CS4PEntities();
            LoaiSanPham edit = db.LoaiSanPhams.First();
            edit.TenLoai = "Tennn";

            using (var scope = new TransactionScope())
            {
                var result1 = controller.Edit(db.LoaiSanPhams.First().id) as ViewResult;
                Assert.IsNotNull(result1);
                Assert.IsTrue(edit.TenLoai.ToString().Equals("Tennn"));
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            var controller = new LoaiSanPhamController();
            var db = new CS4PEntities();

            var result1 = controller.Delete(db.LoaiSanPhams.First().id) as ViewResult;
            Assert.IsNotNull(result1);

            int id = 1;

            ActionResult actual;
            LoaiSanPham delete = db.LoaiSanPhams.Find(id);
            actual = controller.Delete(id);
            Assert.IsFalse(db.LoaiSanPhams.Contains(delete));
        }
    }
}
